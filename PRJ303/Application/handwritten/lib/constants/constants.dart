import 'package:flutter/material.dart';

const double canvasSize = 370;
const double borderSize = 4;
const double strokeWidth = 4;
const double mnistSize = 150;
final Color? backgroundColor = Colors.grey[200];
