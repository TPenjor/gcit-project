import 'dart:io';
import 'package:handwritten/navbar/navbar.dart';
import 'package:handwritten/screen/predicited_screen.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:tflite/tflite.dart';

class UploadImage extends StatefulWidget {
  const UploadImage({Key? key}) : super(key: key);

  @override
  State<UploadImage> createState() => _UploadImageState();
}

class _UploadImageState extends State<UploadImage> {
  File? _image;
  List<dynamic>? _outputs;
  String _name = "";
  String _confidence = "";

  @override
  void initState() {
    super.initState();
    loadModel().then((value) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    Tflite.close();
  }

  //Load the Tflite model
  loadModel() async {
    await Tflite.loadModel(
        model: "assets/handwritten.tflite", labels: "assets/labels.txt");
  }

  classifyImage(image) async {
    var output = await Tflite.runModelOnImage(
      path: image.path,
      numResults: 31,
      imageMean: 0.0,
      imageStd: 255.0,
      threshold: 0.1,
      asynch: true,
    );
    setState(() {
      //Delcare List _outputs in the class which will be used to
      //show the classified class name and confidence
      _outputs = output;
      String str = _outputs![0]['label'];
      _name = str.substring(2);
      _confidence = _outputs != null
          ? (_outputs![0]['confidence'] * 100.0).toString().substring(0, 2)
          : "";
    });
    // ignore: use_build_context_synchronously
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) =>
                PredictedScreen(name: _name, confidence: _confidence)));
  }

  pickImage() async {
    final image = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (image == null) return null;
    setState(() {
      _image = File(image.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: const NavBar(),
        appBar: AppBar(
          titleSpacing: 1,
          centerTitle: false,
          title: const Text(
            "Dzongkha Handwritten Character",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18.0,
              fontFamily: 'Roboto-Thin',
              color: Colors.white,
            ),
          ),
          backgroundColor: const Color.fromARGB(255, 61, 85, 222),
          elevation: 0,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: pickImage,
          backgroundColor: Colors.blue,
          child: const Icon(Icons.image),
        ),
        body: _image == null
            ? Container(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 50,
                    ),
                    Text(
                      "Please pick images from your gallery",
                      style: TextStyle(
                        fontSize: 18.0,
                        fontFamily: 'Roboto-Thin',
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
              )
            : Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: SizedBox(
                      height: 400 + 4 * 2,
                      width: 400 + 4 * 2,
                      child: Image.file(
                        _image!,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 40,
                          width: 150,
                          child: TextButton(
                            style: TextButton.styleFrom(
                                primary: Colors.white,
                                backgroundColor: Colors.blue,
                                onSurface: Colors.grey),
                            onPressed: () {
                              classifyImage(_image);
                            },
                            child: const Text(
                              "Predict",
                              style: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Roboto-Thin',
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ));
  }
}
