import 'package:flutter/material.dart';
import 'package:handwritten/navbar/navbar.dart';
import 'package:handwritten/screen/drawing_page.dart';
import 'package:handwritten/screen/upload_image.dart';

class ChoiceScreen extends StatelessWidget {
  const ChoiceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: const NavBar(),
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 61, 85, 222),
          elevation: 0,
          titleSpacing: 1,
        ),
        body: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/bg.png"), fit: BoxFit.cover)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const SizedBox(
                height: 350,
              ),
              IconButton(
                icon: const Icon(Icons.image),
                iconSize: 90,
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const UploadImage()));
                },
              ),
              const Center(
                child: Text(
                  "Please choose an image from your gallery",
                  style: TextStyle(
                    fontSize: 18.0,
                    fontFamily: 'Roboto-Thin',
                    color: Colors.black,
                  ),
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              IconButton(
                icon: const Icon(Icons.draw_rounded),
                iconSize: 90,
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const DrawPage()));
                },
              ),
              const Center(
                child: Text(
                  "Click here to draw a letter",
                  style: TextStyle(
                    fontSize: 18.0,
                    fontFamily: 'Roboto-Thin',
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
