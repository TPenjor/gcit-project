import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:handwritten/constants/constants.dart';
import 'package:handwritten/navbar/navbar.dart';
import 'package:handwritten/screen/predicited_screen.dart';
import 'dart:ui' as ui;
import 'package:tflite/tflite.dart';
import 'package:path_provider/path_provider.dart';

class DrawPage extends StatefulWidget {
  const DrawPage({Key? key}) : super(key: key);

  @override
  State<DrawPage> createState() => _DrawPageState();
}

class _DrawPageState extends State<DrawPage> {
  GlobalKey globalKey = GlobalKey();
  final pointMode = ui.PointMode.points;
  List<dynamic>? _outputs;
  String _name = "";
  String _confidence = "";
  final _points = <DrawingArea?>[];

  @override
  void initState() {
    super.initState();

    loadModel().then((value) {
      setState(() {});
    });
  }

  Future<void> _save() async {
    final RenderRepaintBoundary boundary =
        globalKey.currentContext!.findRenderObject()! as RenderRepaintBoundary;
    final image = await boundary.toImage();
    final byteData = await image.toByteData(format: ImageByteFormat.png);
    final imageBytes = byteData?.buffer.asUint8List();
    if (imageBytes != null) {
      final directory = await getApplicationDocumentsDirectory();
      final imagePath =
          await File('${directory.path}/container_image.png').create();
      await imagePath.writeAsBytes(imageBytes);
      classifyImage(imagePath);
    }
  }

//Load the Tflite model
  loadModel() async {
    await Tflite.loadModel(
      model: "assets/handwritten.tflite",
      labels: "assets/labels.txt",
    );
  }

  classifyImage(image) async {
    if (image == null) return null;
    var output = await Tflite.runModelOnImage(
        path: image.path,
        numResults: 31,
        threshold: 0.1,
        imageMean: 0.0,
        imageStd: 200.0,
        asynch: true);
    setState(() {
      //Declare List _outputs in the class which will be used to show the classified classs name and confidence
      _outputs = output;
      String str = _outputs![0]['label'];
      _name = str.substring(2);
      _confidence = _outputs != null
          ? (_outputs![0]['confidence'] * 100.0).toString().substring(0, 2)
          : "";
    });
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) =>
                PredictedScreen(name: _name, confidence: _confidence)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavBar(),
      appBar: AppBar(
        titleSpacing: 1,
        centerTitle: false,
        title: const Text(
          "Handwritten Dzongkha Character",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
            fontFamily: 'Roboto-Thin',
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 61, 85, 222),
        elevation: 0,
      ),
      body: Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            const Text("Draw a dzongkha letter",
                style: TextStyle(
                    fontSize: 20,
                    fontFamily: 'Roboto-Thin',
                    color: Colors.black)),
            const SizedBox(
              height: 20,
            ),
            _drawCanvasWidget(),
            const SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 50, //height of button
                  width: 150,
                  child: TextButton(
                    child: const Text(
                      'Predict',
                      style: TextStyle(
                          fontSize: 18.0,
                          fontFamily: 'Roboto-Thin',
                          color: Colors.white),
                    ),
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.blue,
                      onSurface: Colors.grey,
                    ),
                    onPressed: () {
                      _save();
                    },
                  ),
                ),
                const SizedBox(
                  width: 40,
                ),
                SizedBox(
                  height: 50, //height of button
                  width: 150,
                  child: TextButton(
                    child: const Text(
                      'Clear',
                      style: TextStyle(
                          fontSize: 18.0,
                          fontFamily: 'Roboto-Thin',
                          color: Colors.white),
                    ),
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.blue,
                      onSurface: Colors.grey,
                    ),
                    onPressed: () {
                      _points.clear();
                      setState(() {});
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _drawCanvasWidget() {
    return Container(
        width: canvasSize + borderSize * 2,
        height: canvasSize + borderSize * 2,
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black,
            width: borderSize,
          ),
        ),
        child: RepaintBoundary(
          key: globalKey,
          child: GestureDetector(
            onPanDown: (DragDownDetails details) {
              setState(() {
                _points.add(DrawingArea(
                    point: details.localPosition,
                    areaPaint: Paint()
                      ..strokeCap = StrokeCap.round
                      ..isAntiAlias = true
                      ..color = Colors.white
                      ..strokeWidth = strokeWidth));
              });
            },
            onPanUpdate: (DragUpdateDetails details) {
              setState(() {
                _points.add(DrawingArea(
                    point: details.localPosition,
                    areaPaint: Paint()
                      ..strokeCap = StrokeCap.round
                      ..isAntiAlias = true
                      ..color = Colors.white
                      ..strokeWidth = strokeWidth));
              });
            },
            onPanEnd: (DragEndDetails details) async {
              _points.add(null);
              setState(() {});
            },
            child: CustomPaint(
              painter: DrawingPainter(_points),
            ),
          ),
        ));
  }
}

class DrawingArea {
  Offset point;
  Paint areaPaint;

  DrawingArea({required this.point, required this.areaPaint});
}

class DrawingPainter extends CustomPainter {
  final List<DrawingArea?> points;

  DrawingPainter(this.points);

  @override
  void paint(Canvas canvas, Size size) {
    Paint background = Paint()..color = Colors.black;
    Rect rect = Rect.fromLTWH(0, 0, size.width, size.height);
    canvas.drawRect(rect, background);
    canvas.clipRect(rect);

    for (int x = 0; x < points.length - 1; x++) {
      if (points[x] != null && points[x + 1] != null) {
        canvas.drawLine(
            points[x]!.point, points[x + 1]!.point, points[x]!.areaPaint);
      } else if (points[x] != null && points[x + 1] == null) {
        canvas.drawPoints(
            PointMode.points, [points[x]!.point], points[x]!.areaPaint);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
